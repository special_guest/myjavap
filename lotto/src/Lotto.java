import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

abstract class Lotto {
    protected int anzahlKugel;
    protected int anzahlKugelGesamt;
    List<Integer> zahlen = new ArrayList<Integer>();

    Random rand = new Random();

    public void ziehen() {
        while (zahlen.size() < anzahlKugel) {
            int gezogeneKugel = rand.nextInt(anzahlKugelGesamt -1) + 1;

            // Zahlen müssen einzigartig sein
            if (zahlen.contains(gezogeneKugel)) {
                continue;
            }
            zahlen.add(gezogeneKugel);

            Collections.sort(zahlen);
        }
    }

    public void printZahlen() {
        System.out.println("Spiel " + anzahlKugel + " aus " + anzahlKugelGesamt + ". " + zahlen);
    }

    public List getZahlen() {
        return zahlen;
    }

}
