public class LottoTipp extends Lotto {

    public LottoTipp(int anzahlKugel, int anzahlKugelGesamt) {
        this.anzahlKugel = anzahlKugel;
        this.anzahlKugelGesamt = anzahlKugelGesamt;
    }

    public void abgeben () {
        ziehen();
    }
}
