import java.util.ArrayList;
import java.util.List;

public class LottoSpielSimulation {
    static int tippCost = 1;

    public static void main(String[] args) {

        int anzahlKugel = 6;
        int anzahlKugelGesamt = 49;

        autoplay(100, anzahlKugel, anzahlKugelGesamt);

    }

    public static int vergleichen(LottoSpiel lottoSpiel, LottoTipp lottoTipp) {
        List<Integer> tipps = lottoTipp.getZahlen();
        List<Integer> zahlen = lottoSpiel.getZahlen();
        int gewinne = 0;

        for (int tipp : tipps) {
            if (zahlen.contains(tipp)) {
                gewinne++;
            }
        }
        return gewinne;
    }

    public static long geldgewinnBerechnen (int richtige) {
        if (richtige > 0) {
            return (long) Math.pow(10, richtige -1);
        } else {
            return 0;
        }

    }

    public static void autoplay (int repetitions, int anzahlKugel, int anzahlKugelGesamt) {
        long cash = 0;

        for (int i=0; i<repetitions; i++) {
            cash = cash - anzahlKugel * tippCost;

            LottoSpiel lottoSpiel = new LottoSpiel(anzahlKugel, anzahlKugelGesamt);
            lottoSpiel.ziehen();

            LottoTipp lottoTipp = new LottoTipp(anzahlKugel, anzahlKugelGesamt);
            lottoTipp.abgeben();

            int richtige = vergleichen(lottoSpiel, lottoTipp);
            long geldgewinn = geldgewinnBerechnen(richtige);

            cash = cash + geldgewinn;
        }

        System.out.println(cash);
    }
}
